#ifndef SET_H
#define SET_H

#include "AVL.h"

template <typename T>
class Set {
 private:
  shared_ptr<AVL<T>> avl;

 public:
  Set() { avl = make_shared<AVL<T>>(); }

  void insert(T item) {
    avl->insert(item);
  }

  void remove(T item) {
    avl->remove(item);
  }

  bool find(T item) {
    return avl->find(item);
  }

  void display() {
    avl->display();
  }

  string saveToString() {
    return avl->saveToString();
  }

  shared_ptr<Set<T>> map(function<T(const T&)> f) {
    auto navl = avl->map(f);
    auto ns = make_shared<Set<T>>();
    ns->avl = navl;
  }

  shared_ptr<Set<T>> where(function<T(const T&)> f) {
    auto navl = avl->where(f);
    auto ns = make_shared<Set<T>>();
    ns->avl = navl;
  }

  void unionSets(shared_ptr<Set<T>> ns) {
    shared_ptr<T[]> values = ns->getValues();
    int sz = ns->getSize();
    for (int i = 0; i < sz; i++) {
      if (!avl->find(values[i])) {
        avl->insert(values[i]);
      }
    }
  }

  void intersect(shared_ptr<Set<T>> ns) {
    shared_ptr<T[]> values = avl->getValues();
    int sz = avl->getSize();
    for (int i = 0; i < sz; i++) {
      if (!ns->find(values[i])) {
        avl->remove(values[i]);
      }
    }
  }

  void subtract(shared_ptr<Set<T>> ns) {
    shared_ptr<T[]> values = avl->getValues();
    int sz = avl->getSize();
    for (int i = 0; i < sz; i++) {
      if (ns->find(values[i])) {
        avl->remove(values[i]);
      }
    }
  }

  bool isSubset(shared_ptr<Set<T>> ns) {
    shared_ptr<T[]> values = avl->getValues();
    int sz = avl->getSize();
    for (int i = 0; i < sz; i++) {
      if (!ns->find(values[i])) {
        return false;
      }
    }
    return true;
  }

  bool isEqual(shared_ptr<Set<T>> ns) {
    shared_ptr<T[]> values1 = avl->getValues();
    int sz1 = avl->getSize();
    shared_ptr<T[]> values2 = avl->getValues();
    int sz2 = avl->getSize();
    if (sz1 != sz2) {
      return false;
    }
    for (int i = 0; i < sz1; i++) {
      if (values1[i] != values2[i]) {
        return false;
      }
    }
    return true;
  }

};

#endif /*SET_H*/
