#ifndef AVL_H
#define AVL_H

#include <bits/stdc++.h>

using namespace std;

template <typename T>
class AVL {
 private:
  struct node {
    T value;
    shared_ptr<node> left;
    shared_ptr<node> right;
    int height;
  };

  shared_ptr<node> root;
  int height(shared_ptr<node> p) const;
  int getBalance(shared_ptr<node> p) const;

  shared_ptr<node> rotateLL(shared_ptr<node>& p) {
    shared_ptr<node> np = p->left;
    p->left = np->right;
    np->right = p;
    p->height = max(height(p->left), height(p->right)) + 1;
    np->height = max(height(np->left), p->height) + 1;
    return np;
  }

  shared_ptr<node> rotateRR(shared_ptr<node>& p) {
    shared_ptr<node> np = p->right;
    p->right = np->left;
    np->left = p;
    p->height = max(height(p->left), height(p->right)) + 1;
    np->height = max(height(np->right), p->height) + 1;
    return np;
  }

  shared_ptr<node> rotateLR(shared_ptr<node>& p) {
    p->left = rotateRR(p->left);
    return rotateLL(p);
  }

  shared_ptr<node> rotateRL(shared_ptr<node>& p) {
    p->right = rotateLL(p->right);
    return rotateRR(p);
  }

  shared_ptr<node> balance(shared_ptr<node> p) {
    if (height(p->left) - height(p->right) == 2) {
      if (height(p->left->left) - height(p->left->right) == 1) {
        p = rotateLL(p);
      } else {
        p = rotateLR(p);
      }
    } else if (height(p->right) - height(p->left) == 2) {
      if (height(p->right->right) - height(p->right->left) == 1) {
        p = rotateRR(p);
      } else {
        p = rotateRL(p);
      }
    }
    return p;
  }

  shared_ptr<node> insert(shared_ptr<node> p, T item) {
    if (p == nullptr) {
      p = make_shared<node>();
      p->value = item;
      p->height = 1;
      p->left = nullptr;
      p->right = nullptr;
    } else if (item < p->value) {
      p->left = insert(p->left, item);
    } else if (item > p->value) {
      p->right = insert(p->right, item);
    }
    p = balance(p);
    p->height = max(height(p->left), height(p->right)) + 1;
    return p;
  }

  shared_ptr<node> findMin(shared_ptr<node> p) const {
    if (p == nullptr) {
      return nullptr;
    } else if (p->left == nullptr) {
      return p;
    } else {
      return findMin(p->left);
    }
  }

  shared_ptr<node> findMax(shared_ptr<node> p) const {
    if (p == nullptr) {
      return nullptr;
    } else if (p->right == nullptr) {
      return p;
    } else {
      return findMax(p->right);
    }
  }

  shared_ptr<node> remove(T item, shared_ptr<node> p) {
    shared_ptr<node> temp;
    if (p == nullptr) {
      return nullptr;
    } else if (item < p->value) {
      p->left = remove(item, p->left);
    } else if (item > p->value) {
      p->right = remove(item, p->right);
    } else if (p->left && p->right) {
      temp = findMin(p->right);
      p->value = temp->value;
      p->right = remove(p->value, p->right);
    } else {
      temp = p;
      if (p->left == nullptr)
        p = p->right;
      else if (p->right == nullptr)
        p = p->left;
    }
    if (p == nullptr) {
      return p;
    }
    p = balance(p);
    p->height = max(height(p->left), height(p->right)) + 1;
    return p;
  }

  void printByOrder(shared_ptr<node> p) const {
    if (p == nullptr) {
      return;
    }
    printByOrder(p->left);
    cout << p->value << ' ';
    printByOrder(p->right);
  }

  shared_ptr<node> nodeFind(shared_ptr<node> p, T item) const {
    if (p == nullptr) {
      return nullptr;
    }
    if (p->value == item) {
      return p;
    }
    if (item > p->value) {
      return nodeFind(p->right, item);
    } else {
      return nodeFind(p->left, item);
    }
  }

  shared_ptr<node> nodeMap(shared_ptr<node> p, function<T(const T&)> f) const {
    if (p == nullptr) {
      return nullptr;
    }
    auto np = make_shared<node>();
    np->value = f(p->value);
    np->height = p->height;
    np->left = nodeMap(p->left, f);
    np->right = nodeMap(p->right, f);
    return np;
  }

  void nodeWhere(shared_ptr<AVL<T>>& avl, shared_ptr<node> p,
                 function<bool(const T&)> f) const {
    if (p == nullptr) {
      return;
    }
    if (f(p->value)) {
      avl->insert(p->value);
    }
    if (p->left) {
      nodeWhere(avl, p->left, f);
    }
    if (p->right) {
      nodeWhere(avl, p->right, f);
    }
  }

  void nodeSaveToString(string& s, shared_ptr<node> p) {
    if (p == nullptr) {
      s += "null";
      return;
    }
    s += '(';
    nodeSaveToString(s, p->left);
    s += ',';
    s += to_string(p->value);
    s += ',';
    nodeSaveToString(s, p->right);
    s += ')';
  }

  shared_ptr<node> buildSubtree(shared_ptr<node> p) {
    if (p == nullptr) {
      return nullptr;
    }
    auto newp = make_shared<node>();
    newp->value = p->value;
    newp->left = buildSubtree(p->left);
    newp->right = buildSubtree(p->right);
    return newp;
  }

  void nodeSearchSubtree(bool& ans, shared_ptr<node> p, shared_ptr<node> np) {
    if (p == nullptr && np == nullptr) {
      return;
    }
    if ((p == nullptr && np) || (p && np == nullptr)) {
      ans = false;
      return;
    }
    if (p->value != np->value) {
      ans = false;
      return;
    }
    nodeSearchSubtree(ans, p->left, np->left);
    nodeSearchSubtree(ans, p->right, np->right);
  }

  void countNodes(shared_ptr<node> p, int& cnt) {
    if (p == nullptr) {
      return;
    }
    cnt++;
    countNodes(p->left, cnt);
    countNodes(p->right, cnt);
  }

  void getNodesValues(shared_ptr<node> p, shared_ptr<T[]> res, int& ind) {
    if (p == nullptr) {
      return;
    }
    getNodesValues(p->left, res, ind);
    res[ind] = p->value;
    ind++;
    getNodesValues(p->right, res, ind);
  }

 public:
  AVL() { root = nullptr; }

  void insert(T item) { root = insert(root, item); }
  void remove(T item) { root = remove(item, root); }
  void display() const { printByOrder(root); }
  bool find(T item) const { return nodeFind(root, item) ? true : false; }

  shared_ptr<AVL<T>> map(function<T(const T&)> f) const {
    auto newAVL = make_shared<AVL<T>>();
    newAVL->root = nodeMap(root, f);
    return newAVL;
  }

  shared_ptr<AVL<T>> where(function<bool(const T&)> f) const {
    auto newAVL = make_shared<AVL<T>>();
    nodeWhere(newAVL, root, f);
    return newAVL;
  }

  string saveToString() {
    string res = "";
    nodeSaveToString(res, root);
    return res;
  }

  shared_ptr<AVL<T>> getSubtree(T value) {
    auto subtree = make_shared<AVL<T>>();
    shared_ptr<node> p = nodeFind(root, value);
    subtree->root = buildSubtree(p);
    return subtree;
  }

  bool searchSubtree(shared_ptr<AVL<T>> t) {
    bool ans = true;
    nodeSearchSubtree(ans, root, t->root);
    return ans;
  }

  int getSize() {
    int ans = 0;
    countNodes(root, ans);
    return ans;
  }

  shared_ptr<T[]> getValues() {
    shared_ptr<T[]> res(new T[getSize()]);
    int ind = 0;
    getNodesValues(root, res, ind);
    return res;
  }
};

template <typename T>
int AVL<T>::height(shared_ptr<node> p) const {
  return (p == nullptr ? 0 : p->height);
}

template <typename T>
int AVL<T>::getBalance(shared_ptr<node> p) const {
  if (p == nullptr) {
    return 0;
  }
  return height(p->left) - height(p->right);
}

#endif
