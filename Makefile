CC = g++
CFLAGS = -Wall -g

all: lab3

lab3: main.o
		$(CC) $(CFLAGS) main.o -o lab3

main.o: main.cpp 
		$(CC) $(CFLAGS) -c main.cpp 
