#ifndef HEAP_H
#define HEAP_H

#include <bits/stdc++.h>

using namespace std;

template <typename T>
class BinaryHeap {
 private:
  vector<T> arr;

  void siftDown(int i) {
    while (2 * i + 1 < arr.size()) {
      int left = 2 * i + 1;
      int right = 2 * i + 2;
      int tmp = left;
      if (right < arr.size() && arr[right] < arr[left]) {
        tmp = right;
      }
      if (arr[i] <= arr[tmp]) {
        break;
      }
      swap(arr[i], arr[tmp]);
      i = tmp;
    }
  }
  void siftUp(int i) {
    while (arr[i] < arr[(i - 1) / 2]) {
      swap(arr[i], arr[(i - 1) / 2]);
      i = (i - 1) / 2;
    }
  }

 public:
  BinaryHeap() { arr = vector<T>(); }

  T extractMin() {
    T min = arr[0];
    swap(arr[0], arr[arr.size() - 1]);
    arr.pop_back();
    siftDown(0);
    return min;
  }

  void insert(T item) {
    arr.push_back(item);
    siftUp(arr.size() - 1);
  }
};

#endif /*HEAP_H*/
