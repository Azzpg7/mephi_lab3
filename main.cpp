#include <bits/stdc++.h>

#include <functional>

#include "AVL.h"
#include "set.h"
#include "heap.h"

using namespace std;

mt19937 rnd(chrono::steady_clock::now().time_since_epoch().count());

int func(const int& a) { return a + 1; }

bool f(const int& a) { return a > 20; }

void showtests() {
  printf("\e[1;1H\e[2J");
  cout << "Let's create AVL1 and insert 20, 5, 25, 15, 10, 30, 89\n";
  auto t = make_shared<AVL<int>>();
  t->insert(20);
  t->insert(5);
  t->insert(25);
  t->insert(15);
  t->insert(10);
  t->insert(30);
  t->insert(89);
  cout << "Result: ";
  t->display();
  cout << "\n\n";
  cout << "Let's remove 5, 65, 89, 15 from it\n";
  t->remove(5);
  t->remove(65);
  t->remove(89);
  t->remove(15);
  cout << "Result: ";
  t->display();
  cout << "\n\n";
  cout << "Let's check if we have 5 in our AVL1 now\n";
  if (t->find(5)) {
    cout << "Yep!\n";
  } else {
    cout << "Nope\n";
  }
  cout << '\n';
  cout << "Now, let's save our tree to string and see it's structure\n";
  string s = t->saveToString();
  cout << s << "\n\n";
  cout << "Create AVL2 using map() and increasing elements by 1\n";
  auto newAVL = t->map(func);
  cout << "Result: ";
  newAVL->display();
  cout << "\n\n";
  cout << "Create AVL3 using where() and save only elements greater than 20\n";
  auto nnewAVL = newAVL->where(f);
  cout << "Result: ";
  nnewAVL->display();
  cout << "\n\n";
  cout << "Getting subtree by element 30\n";
  auto AVL2 = t->getSubtree(30);
  cout << "Result: ";
  AVL2->display();
  cout << "\n\n";
  cout << "Now, let's same AVL3 and AVL4 and see if we have AVL4 as subtree of AVL3\n";
  auto a1 = make_shared<AVL<int>>();
  auto a2 = make_shared<AVL<int>>();
  a1->insert(1);
  a2->insert(1);
  a1->insert(4);
  a2->insert(4);
  a1->insert(2);
  a2->insert(2);
  a1->insert(3);
  a2->insert(3);
  if (a1->searchSubtree(a2)) {
    cout << "Hooray!\n";
  }
  cout << "\nSet time testing on n = 1e4\n";
  clock_t time = clock();
  auto set11 = make_shared<Set<int>>();
  for (int i = 0; i < 1e4; i++) {
    int tmp = rnd(); 
    set11->insert(tmp); 
  }
  time = clock() - time;
  printf("It took %f seconds to create Set<int> with 1e4 elements\n", ((float)time) / CLOCKS_PER_SEC);
  /*
  cout << "\nSet time testing on n = 1e6\n";
  time = clock();
  auto set12 = make_shared<Set<int>>();
  for (int i = 0; i < 1e6; i++) {
    int tmp = rnd(); 
    set12->insert(tmp); 
  }
  time = clock() - time;
  printf("It took %f seconds to create Set<int> with 1e6 elements\n----------\n", ((float)time) / CLOCKS_PER_SEC);
  */
  BinaryHeap<int> heap;
  heap.insert(2);
  heap.insert(7);
  heap.insert(4);
  heap.insert(1);
  heap.insert(8);
  heap.insert(9);
  for (int i = 0; i < 6; i++) {
    cout << heap.extractMin() << '\n';
  }
  
}

int main() {
  showtests();
}
